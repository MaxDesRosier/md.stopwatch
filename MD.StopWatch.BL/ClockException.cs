﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MD.StopWatch.BL
{
    public class ClockException : Exception
    {
        public string TimerRunningError
        { get
            {
                return "The timer is already Running!";
            }
        }
        public string TimerStoppedError
        {
            get
            {
                return "The timer is already Stopped!";
            }
        }
        //ClockException(string error)
        //{
            
        //}
    }
}
