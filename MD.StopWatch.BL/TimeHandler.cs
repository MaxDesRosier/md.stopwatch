﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MD.StopWatch.BL
{
    public class TimeHandler
    {
        public bool TimerRunning = false;
        /********** Fields **********/
        private DateTime startTime;
        private DateTime stopTime;
        private TimeSpan elapsedTime; 
        /******** Properties ********/
        public string ElapsedTime
        { 
            get
            {
                if (TimerRunning == true)
                {
                    elapsedTime = DateTime.Now - startTime;
                }
                else
                {
                    elapsedTime = stopTime - startTime;
                }
                return elapsedTime.ToString(@"hh\:mm\:ss");
            }
        }
        /********** Methods *********/
        public void StartClock()
        {
            if (TimerRunning == true) throw new ClockException();
            startTime = DateTime.Now;
            TimerRunning = true;
        }
        public void StopClock()
        {
            if (TimerRunning == false) throw new ClockException();
            stopTime = DateTime.Now;
            TimerRunning = false;
        }
        /********** Constructors *********/

    }
}
