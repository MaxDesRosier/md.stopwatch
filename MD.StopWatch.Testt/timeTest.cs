﻿using System;
using MD.StopWatch.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MD.StopWatch.Testt
{
    [TestClass]
    public class TimeTest
    {
        
        [TestMethod]
        public void StartClockTest()
        {
            TimeHandler t = new TimeHandler();
            Assert.IsFalse(t.TimerRunning);
        }
        public void StopClockTest()
        {
            TimeHandler t = new TimeHandler();
            Assert.IsTrue(t.TimerRunning);
        }

    }
}
