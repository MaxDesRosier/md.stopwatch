﻿using MD.StopWatch.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MD.StopWatch
{
    public partial class Form1 : Form
    {
        TimeHandler t = new TimeHandler();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                lblSplitTime.Text = t.ElapsedTime;
                tmrTimer.Start();
                t.StartClock();
            }
            catch(ClockException ce)
            {
                MessageBox.Show(ce.TimerRunningError);
            }
            //btnStart.Enabled = false;
            //btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                tmrTimer.Stop();
                t.StopClock();
                lblTimeElapsed.Text = t.ElapsedTime;
                lblSplitTime.Text = t.ElapsedTime;
            }
            catch (ClockException ce)
            {
                MessageBox.Show(ce.TimerStoppedError);
            }//btnStart.Enabled = true;
            //btnStop.Enabled = false;            
        }

        private void tmrTimer_Tick(object sender, EventArgs e)
        {
            lblTimeElapsed.Text = t.ElapsedTime;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
